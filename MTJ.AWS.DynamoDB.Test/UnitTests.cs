using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.Runtime;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using MTJ.AWS.DynamoDB;
using System;

namespace MTJ.AWS.DynamoDB.Test
{
    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public void Query()
        {
            string query = new PartiQLQuery()
                .From("TABLE")
                .Where("CONDITION")
                .ToString();
            Assert.AreEqual(query.ToString(), "select * from \"TABLE\" where CONDITION");
        }

        [TestMethod]
        public void Select()
        {
            string query = new PartiQLQuery()
                .From("TABLE")
                .Where<Person>(p => p.Name == "NAME")
                .ToString();
            Assert.AreEqual(query.ToString(), "select * from \"TABLE\" where name = 'NAME'");
        }

        [TestMethod]
        public void SelectGeneric()
        {
            string query = new PartiQLQuery<Person>()
                .From("TABLE")
                .Where(p => p.Name == "NAME")
                .ToString();
            Assert.AreEqual(query.ToString(), "select * from \"TABLE\" where name = 'NAME'");
        }

        [TestMethod]
        public void SelectGenericGuid()
        {
            var guid = Guid.NewGuid();
            string query = new PartiQLQuery<Person>()
                .From("TABLE")
                .Where(p => p.Id == guid)
                .ToString();
            Assert.AreEqual(query.ToString(), $"select * from \"TABLE\" where id = '{guid}'");
        }
    }

    public class Person
    {
        [PartiQLName("id")]
        public Guid Id;
        [PartiQLName("name")]
        public string Name;
    }
}
