using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.Runtime;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace MTJ.AWS.DynamoDB.Test
{
    [TestClass]
    public class IntegrationTests
    {
        private static AwsDynamoDbTableClient<User> Table;

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            var client = new AwsDynamoDbClient();
            Table = client.ConstructTableClient<User>("mst-favorite-authors");
        }

        [TestMethod]
        public async Task Fetch()
        {
            AwsDynamoDbScanResult<User> users = await Table.Scan();
            Assert.IsTrue(users.Data.Length > 0);
        }

        [TestMethod]
        public async Task GetNull()
        {
            User user = await Table.GetItem("asd");
            Assert.IsNull(user);
        }

        [TestMethod]
        public async Task Get()
        {
            User[] users = await Table.GetItems(new Primitive[] { "Annoyingjaina", "BaileyBellBaileyBell" });
            Assert.IsTrue(users.Length > 0);
        }

        [TestMethod]
        public async Task QueryByString()
        {
            User[] users = await Table.Query($"username = 'teddybearsandflairs'");
            Assert.IsTrue(users.Length > 0);
        }

        [TestMethod]
        public async Task QueryByExpression()
        {
            User[] users = await Table.Query(u => u.Username == "-LittleMissSarah-");
            Assert.IsTrue(users.Length > 0);
        }

        private class User
        {
            [PartiQLName("username")]
            public string Username;
        }
    }
}
