﻿using Amazon.DynamoDBv2;
using Amazon.Runtime;
using Microsoft.Extensions.Configuration;
using MTJ.AWS.Core;
using System;

namespace MTJ.AWS.DynamoDB
{
    public class AwsDynamoDbClient : AwsClientBase<IAmazonDynamoDB>
    {
        public AwsDynamoDbClient(): base() { }
        public AwsDynamoDbClient(IConfiguration configuration): base(configuration) { }
        public AwsDynamoDbClient(string accessKey, string secretKey, string region): base(accessKey, secretKey, region) { }

        public AwsDynamoDbTableClient ConstructTableClient(string tableName)
        {
            return new AwsDynamoDbTableClient(tableName, ServiceClient);
        }

        public AwsDynamoDbTableClient<T> ConstructTableClient<T>(string tableName)
        {
            return new AwsDynamoDbTableClient<T>(tableName, ServiceClient);
        }
    }
}
