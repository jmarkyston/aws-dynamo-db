﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MTJ.AWS.DynamoDB
{
    public class AwsDynamoDbTableClient
    {
        private string TableName;
        private IAmazonDynamoDB Client;

        private Table _Table;
        private Table Table
        {
            get
            {
                if (_Table == null)
                    _Table = Table.LoadTable(Client, TableName);
                return _Table;
            }
        }

        private T DeserializeDocument<T>(Document document)
        {
            return JsonConvert.DeserializeObject<T>(document.ToJson());
        }
        private Document SerializeDocument(object document)
        {
            string json = JsonConvert.SerializeObject(document, new JsonSerializerSettings()
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });
            return Document.FromJson(json);
        }

        public AwsDynamoDbTableClient(string tableName, IAmazonDynamoDB client)
        {
            TableName = tableName;
            Client = client;
        }

        public async Task<T> GetItem<T>(Primitive key)
        {
            Document document = await Table.GetItemAsync(key);
            return document == null ? default : DeserializeDocument<T>(document);
        }
        public async Task<T[]> GetItems<T>(IEnumerable<Primitive> keys)
        {
            DocumentBatchGet batch = Table.CreateBatchGet();
            foreach (Primitive key in keys)
            {
                batch.AddKey(key);
            }
            await batch.ExecuteAsync();
            return batch.Results.Select(d => d == null ? default : DeserializeDocument<T>(d)).ToArray();
        }

        public async Task<AwsDynamoDbScanResult<T>> Scan<T>(int? limit = null, string paginationToken = null)
        {
            var config = new ScanOperationConfig();
            if (limit.HasValue)
                config.Limit = limit.Value;
            if (paginationToken != null)
                config.PaginationToken = paginationToken;
            Search search = Table.Scan(config);
            List<Document> documents;
            if (limit.HasValue)
                documents = await search.GetNextSetAsync();
            else
                documents = await search.GetRemainingAsync();
            T[] data = documents.Select(d => DeserializeDocument<T>(d)).ToArray();
            return new AwsDynamoDbScanResult<T>(data, search.PaginationToken);
        }

        public async Task<T[]> QueryIndex<T, V>(string index, string key, V value)
        {
            string expression = $"{key} = :value";
            Type type = typeof(V);
            string attrVal = value.ToString();
            var attr = new AttributeValue();
            if (typeof(string).IsAssignableFrom(type) || typeof(Guid).IsAssignableFrom(type))
                attr.S = attrVal;
            else if (typeof(int).IsAssignableFrom(type))
                attr.N = attrVal;
            var expressionValues = new Dictionary<string, AttributeValue>()
            {
                { ":value", attr }
            };
            var request = new QueryRequest()
            {
                TableName = TableName,
                IndexName = index,
                KeyConditionExpression = expression,
                ExpressionAttributeValues = expressionValues
            };
            QueryResponse response = await Client.QueryAsync(request);
            return response.Items
                .Select(i => Document.FromAttributeMap(i))
                .Select(d => DeserializeDocument<T>(d))
                .ToArray();
        }

        public async Task<T[]> Query<T>(string statement)
        {
            statement = $"select * from \"{TableName}\" where {statement}";
            var request = new ExecuteStatementRequest()
            {
                Statement = statement
            };
            ExecuteStatementResponse response = await Client.ExecuteStatementAsync(request);
            return response.Items
                .Select(i => Document.FromAttributeMap(i))
                .Select(d => DeserializeDocument<T>(d))
                .ToArray();
        }
        public async Task<T[]> Query<T>(Expression<Func<T, bool>> expression)
        {
            return await new AwsDynamoDbPartiQLQuery(this, PartiQL.ALL_TARGET)
                .From(TableName)
                .Where(expression)
                .Execute<T>();
        }

        public async Task PutItem<T>(T item)
        {
            Document document = SerializeDocument(item);
            await Table.PutItemAsync(document);
        }

        private async Task ExecuteBatchWrite<T>(IEnumerable<T> items, Action<DocumentBatchWrite, Document> action)
        {
            DocumentBatchWrite batch = Table.CreateBatchWrite();
            foreach (T item in items)
            {
                Document doc = SerializeDocument(item);
                action(batch, doc);
            }
            await batch.ExecuteAsync();
        }
        public async Task PutItems<T>(IEnumerable<T> items)
        {
            await ExecuteBatchWrite(items, (batch, doc) =>
            {
                batch.AddDocumentToPut(doc);
            });
        }
        public async Task DeleteItems<T>(IEnumerable<T> items)
        {
            await ExecuteBatchWrite(items, (batch, doc) =>
            {
                batch.AddItemToDelete(doc);
            });
        }

        public async Task DeleteItem(Primitive key)
        {
            await Table.DeleteItemAsync(key);
        }
    }
}
