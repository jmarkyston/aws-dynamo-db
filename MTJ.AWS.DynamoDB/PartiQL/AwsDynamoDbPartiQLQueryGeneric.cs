﻿using System;
using System.Threading.Tasks;

namespace MTJ.AWS.DynamoDB
{
    public class AwsDynamoDbPartiQLQuery<T> : PartiQLQuery<T>
    {
        private AwsDynamoDbTableClient<T> Client;

        public AwsDynamoDbPartiQLQuery(AwsDynamoDbTableClient<T> client, string target = PartiQL.ALL_TARGET) : base(target)
        {
            Client = client;
        }

        public override async Task<T[]> Execute()
        {
            return await Client.Query(ToString());
        }
    }
}
