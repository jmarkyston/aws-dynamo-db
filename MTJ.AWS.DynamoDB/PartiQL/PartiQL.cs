﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace MTJ.AWS.DynamoDB
{
    public class PartiQL
    {
        public const string ALL_TARGET = "*";

        public static string TranslateExpression(Expression expression)
        {
            switch (expression.NodeType)
            {
                case ExpressionType.Lambda: return TranslateLambdaExpression((LambdaExpression)expression);
                case ExpressionType.MemberAccess: return TranslateMemberExpression((MemberExpression)expression);
                case ExpressionType.Constant: return TranslateConstantExpression((ConstantExpression)expression);
                case ExpressionType.Call: return TranslateMethodCallExpression((MethodCallExpression)expression);
                default: throw new NotSupportedException($"Expression type {expression.NodeType} not supported.");
            }
        }

        private static string TranslateLambdaExpression(LambdaExpression expression)
        {
            switch (expression.Body.NodeType)
            {
                case ExpressionType.Equal: return TranslateBinaryExpression((BinaryExpression)expression.Body);
                default: throw new NotSupportedException($"Lambda type {expression.Body.NodeType} not supported.");
            }
        }

        private static string TranslateBinaryExpression(BinaryExpression expression)
        {
            string left = TranslateExpression(expression.Left);
            string right = TranslateExpression(expression.Right);
            string op = TranslateExpressionOperator(expression.NodeType);
            return $"{left} {op} {right}";
        }

        private static string TranslateMemberExpression(MemberExpression expression)
        {
            string name = expression.Member.Name;
            CustomAttributeData attribute = expression.Member.CustomAttributes
                .FirstOrDefault(a => a.AttributeType == typeof(PartiQLNameAttribute));
            if (attribute != null)
                name = attribute.ConstructorArguments[0].Value.ToString();
            return name;
        }

        private static string ParseValue(object value)
        {
            if (value is string)
                return $"'{value}'";
            else
                return value.ToString();
        }

        private static string TranslateConstantExpression(ConstantExpression expression)
        {
            return ParseValue(expression.Value);
        }

        private static string TranslateMethodCallExpression(MethodCallExpression expression)
        {
            object value = Expression.Lambda(expression).Compile().DynamicInvoke().ToString();
            return ParseValue(value);
        }

        private static string TranslateExpressionOperator(ExpressionType type)
        {
            switch (type)
            {
                case ExpressionType.Equal: return "=";
                default: throw new NotSupportedException($"Operator type {type} not supported.");
            }
        }
    }
}
