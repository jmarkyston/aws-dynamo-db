﻿using System;
namespace MTJ.AWS.DynamoDB
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public class PartiQLNameAttribute : Attribute
    {
        private string _Name;
        public string Name
        {
            get => _Name;
            set => _Name = value;
        }

        public PartiQLNameAttribute(string name)
        {
            _Name = name;
        }
    }
}
