﻿using System;
using System.Threading.Tasks;

namespace MTJ.AWS.DynamoDB
{
    public class AwsDynamoDbPartiQLQuery : PartiQLQuery
    {
        private AwsDynamoDbTableClient Client;

        public AwsDynamoDbPartiQLQuery(AwsDynamoDbTableClient client, string target = PartiQL.ALL_TARGET) : base(target)
        {
            Client = client;
        }

        public override async Task<T[]> Execute<T>()
        {
            return await Client.Query<T>(ToString());
        }
    }
}
