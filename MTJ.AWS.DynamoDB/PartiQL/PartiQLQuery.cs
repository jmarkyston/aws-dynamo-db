﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MTJ.AWS.DynamoDB
{
    public class PartiQLQuery
    {
        private List<string> Components;

        public PartiQLQuery(string target = PartiQL.ALL_TARGET)
        {
            Components = new List<string>()
            {
                $"select {target}"
            };
        }

        private PartiQLQuery AddComponent(string component)
        {
            Components.Add(component);
            return this;
        }

        public PartiQLQuery From(string table)
        {
            return AddComponent($"from \"{table}\"");
        }

        public PartiQLQuery Where(string condition)
        {
            return AddComponent($"where {condition}");
        }
        public PartiQLQuery Where<T>(Expression<Func<T, bool>> expression)
        {
            string translation = PartiQL.TranslateExpression(expression);
            return AddComponent($"where {translation}");
        }

        public override string ToString()
        {
            return string.Join(" ", Components.ToArray());
        }

        public virtual Task<T[]> Execute<T>()
        {
            throw new NotImplementedException();
        }
    }
}