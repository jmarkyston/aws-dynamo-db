﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTJ.AWS.DynamoDB
{
    public class AwsDynamoDbScanResult<T>
    {
        public T[] Data;
        public string PaginationToken;

        public AwsDynamoDbScanResult(T[] data, string paginationToken)
        {
            Data = data;
            PaginationToken = paginationToken;
        }
    }
}
