﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MTJ.AWS.DynamoDB
{
    public class AwsDynamoDbTableClient<T> : IAwsDynamoDbTableClient<T>
    {
        private string TableName;
        private IAmazonDynamoDB Client;

        private Table _Table;
        private Table Table
        {
            get
            {
                if (_Table == null)
                    _Table = Table.LoadTable(Client, TableName);
                return _Table;
            }
        }

        private AwsDynamoDbTableClient _TableClient;
        private AwsDynamoDbTableClient TableClient
        {
            get
            {
                if (_TableClient == null)
                    _TableClient = new AwsDynamoDbTableClient(TableName, Client);
                return _TableClient;
            }
        }

        public AwsDynamoDbTableClient(string tableName, IAmazonDynamoDB client)
        {
            TableName = tableName;
            Client = client;
        }

        public async Task<T> GetItem(Primitive key)
        {
            return await TableClient.GetItem<T>(key);
        }

        public async Task<T[]> GetItems(IEnumerable<Primitive> keys)
        {
            return await TableClient.GetItems<T>(keys);
        }

        public async Task<AwsDynamoDbScanResult<T>> Scan(int? limit = null, string paginationToken = null)
        {
            return await TableClient.Scan<T>(limit, paginationToken);
        }

        public async Task<T[]> QueryIndex<V>(string index, string key, V value)
        {
            return await TableClient.QueryIndex<T, V>(index, key, value);
        }

        public async Task<T[]> Query(string statement)
        {
            return await TableClient.Query<T>(statement);
        }
        public async Task<T[]> Query(Expression<Func<T, bool>> expression)
        {
            return await TableClient.Query<T>(expression);
        }

        public async Task PutItem(T item)
        {
            await TableClient.PutItem(item);
        }

        public async Task PutItems(IEnumerable<T> items)
        {
            await TableClient.PutItems(items);
        }

        public async Task DeleteItem(Primitive key)
        {
            await Table.DeleteItemAsync(key);
        }

        public async Task DeleteItems(IEnumerable<T> items)
        {
            await TableClient.DeleteItems(items);
        }
    }
}
