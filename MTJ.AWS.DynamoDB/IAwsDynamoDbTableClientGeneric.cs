﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;

namespace MTJ.AWS.DynamoDB
{
    public interface IAwsDynamoDbTableClient<T>
    {
        Task<T> GetItem(Primitive key);
        Task<T[]> GetItems(IEnumerable<Primitive> keys);
        Task<AwsDynamoDbScanResult<T>> Scan(int? limit = null, string paginationToken = null);
        Task<T[]> QueryIndex<V>(string index, string key, V value);
        Task<T[]> Query(string statement);
        Task<T[]> Query(Expression<Func<T, bool>> expression);
        Task PutItem(T item);
        Task PutItems(IEnumerable<T> items);
        Task DeleteItem(Primitive key);
        Task DeleteItems(IEnumerable<T> items);
    }
}
